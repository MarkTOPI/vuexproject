import Vue from 'vue'
import Vuex from 'vuex'
import Profile from './Profile/'
Vue.use(Vuex)
export default new Vuex.Store({
    modules: {
        Profile
    }
})
