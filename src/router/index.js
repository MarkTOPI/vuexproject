import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter)
const router  = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/parsing',
            name: 'parsing-page',
            component:() =>
                import('@/pages/Parsing.vue')
        }
    ]
})
export default router